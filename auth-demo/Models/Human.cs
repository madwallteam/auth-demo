﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo.Models
{
    public abstract class Human
    {
        public Guid Id { get; set; }
        [JsonIgnore] public string Login { get;  set; }
        [JsonIgnore] public string Passhash { get;  set; }
        public virtual Rights Right { get; set; }
    }
}
