﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo.Models
{
    public class User : Human
    {
        public override Rights Right => Rights.User;
    }
}
