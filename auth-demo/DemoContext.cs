﻿using auth_demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace auth_demo
{
    public class DemoContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseSqlServer("Server=31.31.196.234;Database=u0588704_auth-demo;User Id=u0588704_auth-demo;Password=Qwerty1234!;");


        public DbSet<User> Users { get; set; }
        public DbSet<Moderator> Moderators { get; set; }
        public DbSet<Human> Humen { get; set; }
    }
}
