﻿using auth_demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo
{
    public static class LocalAuthManager
    {
        private struct Session
        {
            public Human User { get; set; }
            public Guid Token { get; set; }
            public DateTime LastOp { get; set; }

            public bool IsActive => DateTime.Now - LastOp < TimeSpan.FromMinutes(30);
            public Rights Rights => User.Right;
        }
        private static List<Session> Sessions { get; set; } = new();
        public static void UpdateSessions() => Sessions.RemoveAll(x => !x.IsActive);

        public static Guid Authanticate(string login, string password)
        {
            UpdateSessions();
            var passhash = Encrypt.Sha256Hash(password);
            using DemoContext db = new();
            var potentialUser = db.Humen.FirstOrDefault(x => x.Login == login && x.Passhash == passhash);
            if (potentialUser is null)
                throw new UnauthorizedAccessException("Wrong Login-Password pair");

            var newSession = new Session()
            {
                User = potentialUser,
                LastOp = DateTime.Now,
                Token = Guid.NewGuid()
            };
            Sessions.Add(newSession);
            return newSession.Token;
        }
        public static Rights Authorize(Guid token)
        {
            UpdateSessions();
            var potentialUser = Sessions.FirstOrDefault(x => x.Token == token);
            if (!potentialUser.IsActive)
                throw new UnauthorizedAccessException("Session is not valid");
            potentialUser.LastOp = DateTime.Now;
            return potentialUser.Rights;
        }

        public static Human GetSessionUser(Guid token)
        {
            UpdateSessions();
            var potentialUser = Sessions.FirstOrDefault(x => x.Token == token);
            if (!potentialUser.IsActive)
                throw new UnauthorizedAccessException("Session is not valid");
            potentialUser.LastOp = DateTime.Now;
            return potentialUser.User;
        }
    }
}