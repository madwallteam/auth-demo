﻿using auth_demo.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class HumanController : ControllerBase
    {
        [HttpGet("moderators")]
        public IActionResult GetModerators()
        {
            using DemoContext db = new();
            return Ok(new { status = "ok", moderators = db.Moderators.ToList() });           
        }
        [HttpGet("users")]
        public IActionResult GetUsers()
        {
            using DemoContext db = new();
            return Ok(new { status = "ok", moderators = db.Users.ToList() });
        }

        [HttpPost("moderators")]
        public IActionResult PostModerator([FromBody] Moderator moderator)
        {
            using DemoContext db = new();
            moderator.Passhash = Encrypt.Sha256Hash(moderator.Passhash);
            if (moderator.Id == Guid.Empty)
                db.Add(moderator);
            else
                db.Update(moderator);
            db.SaveChanges();
            return Ok(new { status = "ok" });
        }

    }
}
