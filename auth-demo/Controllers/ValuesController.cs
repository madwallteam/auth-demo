﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo.Controllers
{
    [Route("api/testvalue")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public IActionResult Calc(int a, int b, Guid token)
        {
            try
            {
                var rights = LocalAuthManager.Authorize(token);
                if (rights != Rights.Moderator)
                    throw new UnauthorizedAccessException("You have no rights!");
            }
            catch (UnauthorizedAccessException E)
            {
                return Unauthorized(new { status = "fail", message = E.Message });
            }
            catch (Exception E)
            {
                return StatusCode(500, new { status = "fail", message = E.Message, internalMessage = E.InnerException.Message });
            }

            return Ok(new { status = "ok", result = a + b });
        }
    }
}