﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace auth_demo.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpPost]
        public IActionResult Auth(string login, string password)
        {
            try
            {
                var token = LocalAuthManager.Authanticate(login, password);
                return Ok(new { status = "ok", token });
            }
            catch (UnauthorizedAccessException E)
            {
                return Unauthorized(new { status = "fail", message = E.Message });
            }
            catch (Exception E)
            {
                return StatusCode(500, new { status = "fail", message = E.Message, internalMessage = E.InnerException.Message });
            }
        }
    }
}